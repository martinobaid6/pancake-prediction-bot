"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GRAPH_WS_PREDICTIONS = exports.GRAPH_API_PREDICTIONS = exports.GRAPH_API_PROFILE = void 0;
exports.GRAPH_API_PROFILE = process.env.REACT_APP_GRAPH_API_PROFILE;
exports.GRAPH_API_PREDICTIONS = process.env.REACT_APP_GRAPH_API_PREDICTIONS;
exports.GRAPH_WS_PREDICTIONS = process.env.REACT_APP_GRAPH_WS_PREDICTIONS;
//# sourceMappingURL=endpoints.js.map