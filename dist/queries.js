"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUserBaseFields = exports.getBetBaseFields = exports.getRoundBaseFields = void 0;
exports.getRoundBaseFields = () => `
  id
  epoch
  failed
  startAt
  startBlock
  lockAt
  lockBlock
  lockPrice
  endAt
  endBlock
  closePrice
  totalBets
  totalAmount
  bullBets
  bullAmount
  bearBets
  bearAmount
  position
`;
exports.getBetBaseFields = () => `
  id
  hash  
  amount
  position
  claimed
`;
exports.getUserBaseFields = () => `
  id
  address
  block
  totalBets
  totalBNB
`;
//# sourceMappingURL=queries.js.map