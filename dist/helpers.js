"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBet = exports.getBetHistory = exports.getRound = exports.getMarketData = exports.getStaticPredictionsData = exports.getUnclaimedWinningBets = exports.getCanClaim = exports.getRoundResult = exports.makeRoundData = exports.transformMarketResponse = exports.transformRoundResponse = exports.transformBetResponse = exports.makeFutureRoundResponse = exports.numberOrNull = exports.Result = void 0;
const graphql_request_1 = require("graphql-request");
const endpoints_1 = require("config/constants/endpoints");
const types_1 = require("state/types");
const makeBatchRequest_1 = __importDefault(require("utils/makeBatchRequest"));
const contractHelpers_1 = require("utils/contractHelpers");
const queries_1 = require("./queries");
var Result;
(function (Result) {
    Result["WIN"] = "win";
    Result["LOSE"] = "lose";
    Result["CANCELED"] = "canceled";
    Result["LIVE"] = "live";
})(Result = exports.Result || (exports.Result = {}));
exports.numberOrNull = (value) => {
    if (value === null) {
        return null;
    }
    const valueNum = Number(value);
    return Number.isNaN(valueNum) ? null : valueNum;
};
exports.makeFutureRoundResponse = (epoch, startBlock) => {
    return {
        id: epoch.toString(),
        epoch: epoch.toString(),
        startBlock: startBlock.toString(),
        failed: null,
        startAt: null,
        lockAt: null,
        lockBlock: null,
        lockPrice: null,
        endBlock: null,
        closePrice: null,
        totalBets: '0',
        totalAmount: '0',
        bearBets: '0',
        bullBets: '0',
        bearAmount: '0',
        bullAmount: '0',
        position: null,
        bets: [],
    };
};
exports.transformBetResponse = (betResponse) => {
    const bet = {
        id: betResponse.id,
        hash: betResponse.hash,
        amount: betResponse.amount ? parseFloat(betResponse.amount) : 0,
        position: betResponse.position === 'Bull' ? types_1.BetPosition.BULL : types_1.BetPosition.BEAR,
        claimed: betResponse.claimed,
        user: {
            id: betResponse.user.id,
            address: betResponse.user.address,
            block: exports.numberOrNull(betResponse.user.block),
            totalBets: exports.numberOrNull(betResponse.user.totalBets),
            totalBNB: exports.numberOrNull(betResponse.user.totalBNB),
        },
    };
    if (betResponse.round) {
        bet.round = exports.transformRoundResponse(betResponse.round);
    }
    return bet;
};
exports.transformRoundResponse = (roundResponse) => {
    const { id, epoch, failed, startBlock, startAt, lockAt, lockBlock, lockPrice, endBlock, closePrice, totalBets, totalAmount, bullBets, bearBets, bearAmount, bullAmount, position, bets = [], } = roundResponse;
    const getRoundPosition = (positionResponse) => {
        if (positionResponse === 'Bull') {
            return types_1.BetPosition.BULL;
        }
        if (positionResponse === 'Bear') {
            return types_1.BetPosition.BEAR;
        }
        return null;
    };
    return {
        id,
        failed,
        epoch: exports.numberOrNull(epoch),
        startBlock: exports.numberOrNull(startBlock),
        startAt: exports.numberOrNull(startAt),
        lockAt: exports.numberOrNull(lockAt),
        lockBlock: exports.numberOrNull(lockBlock),
        lockPrice: lockPrice ? parseFloat(lockPrice) : null,
        endBlock: exports.numberOrNull(endBlock),
        closePrice: closePrice ? parseFloat(closePrice) : null,
        totalBets: exports.numberOrNull(totalBets),
        totalAmount: totalAmount ? parseFloat(totalAmount) : 0,
        bullBets: exports.numberOrNull(bullBets),
        bearBets: exports.numberOrNull(bearBets),
        bearAmount: exports.numberOrNull(bearAmount),
        bullAmount: exports.numberOrNull(bullAmount),
        position: getRoundPosition(position),
        bets: bets.map(exports.transformBetResponse),
    };
};
exports.transformMarketResponse = (marketResponse) => {
    return {
        id: marketResponse.id,
        paused: marketResponse.paused,
        epoch: Number(marketResponse.epoch.epoch),
    };
};
exports.makeRoundData = (rounds) => {
    return rounds.reduce((accum, round) => {
        return Object.assign(Object.assign({}, accum), { [round.id]: round });
    }, {});
};
exports.getRoundResult = (bet, currentEpoch) => {
    const { round } = bet;
    if (round.failed) {
        return Result.CANCELED;
    }
    if (round.epoch >= currentEpoch - 1) {
        return Result.LIVE;
    }
    const roundResultPosition = round.closePrice > round.lockPrice ? types_1.BetPosition.BULL : types_1.BetPosition.BEAR;
    return bet.position === roundResultPosition ? Result.WIN : Result.LOSE;
};
exports.getCanClaim = (bet) => {
    return !bet.claimed && (bet.position === bet.round.position || bet.round.failed === true);
};
exports.getUnclaimedWinningBets = (bets) => {
    return bets.filter(exports.getCanClaim);
};
exports.getStaticPredictionsData = async () => {
    const { methods } = contractHelpers_1.getPredictionsContract();
    const [currentEpoch, intervalBlocks, minBetAmount, isPaused, bufferBlocks] = await makeBatchRequest_1.default([
        methods.currentEpoch().call,
        methods.intervalBlocks().call,
        methods.minBetAmount().call,
        methods.paused().call,
        methods.bufferBlocks().call,
    ]);
    return {
        status: isPaused ? types_1.PredictionStatus.PAUSED : types_1.PredictionStatus.LIVE,
        currentEpoch: Number(currentEpoch),
        intervalBlocks: Number(intervalBlocks),
        bufferBlocks: Number(bufferBlocks),
        minBetAmount,
    };
};
exports.getMarketData = async () => {
    const response = (await graphql_request_1.request(endpoints_1.GRAPH_API_PREDICTIONS, graphql_request_1.gql `
      query getMarketData {
        rounds(first: 5, orderBy: epoch, orderDirection: desc) {
          ${queries_1.getRoundBaseFields()}
        }
        market(id: 1) {
          id
          paused
          epoch {
            epoch
          }
        }
      }
    `));
    return {
        rounds: response.rounds.map(exports.transformRoundResponse),
        market: exports.transformMarketResponse(response.market),
    };
};
exports.getRound = async (id) => {
    const response = await graphql_request_1.request(endpoints_1.GRAPH_API_PREDICTIONS, graphql_request_1.gql `
      query getRound($id: ID!) {
        round(id: $id) {
          ${queries_1.getRoundBaseFields()}
          bets {
           ${queries_1.getBetBaseFields()}
            user {
             ${queries_1.getUserBaseFields()}
            }
          }
        }
      }
  `, { id });
    return response.round;
};
exports.getBetHistory = async (where = {}, first = 1000, skip = 0) => {
    const response = await graphql_request_1.request(endpoints_1.GRAPH_API_PREDICTIONS, graphql_request_1.gql `
      query getBetHistory($first: Int!, $skip: Int!, $where: Bet_filter) {
        bets(first: $first, skip: $skip, where: $where) {
          ${queries_1.getBetBaseFields()}
          round {
            ${queries_1.getRoundBaseFields()}
          }
          user {
            ${queries_1.getUserBaseFields()}
          } 
        }
      }
    `, { first, skip, where });
    return response.bets;
};
exports.getBet = async (betId) => {
    const response = await graphql_request_1.request(endpoints_1.GRAPH_API_PREDICTIONS, graphql_request_1.gql `
      query getBet($id: ID!) {
        bet(id: $id) {
          ${queries_1.getBetBaseFields()}
          round {
            ${queries_1.getRoundBaseFields()}
          }
          user {
            ${queries_1.getUserBaseFields()}
          } 
        }
      }
  `, {
        id: betId.toLowerCase(),
    });
    return response.bet;
};
//# sourceMappingURL=helpers.js.map