export interface UserResponse {
    id: string;
    address: string;
    block: string;
    totalBets: string;
    totalBNB: string;
    bets?: BetResponse[];
}
export interface BetResponse {
    id: string;
    hash: string;
    amount: string;
    position: string;
    claimed: boolean;
    user?: UserResponse;
    round?: RoundResponse;
}
export interface HistoricalBetResponse {
    id: string;
    hash: string;
    amount: string;
    position: string;
    claimed: boolean;
    user?: UserResponse;
    round: {
        id: string;
        epoch: string;
    };
}
export interface RoundResponse {
    id: string;
    epoch: string;
    failed: boolean;
    startBlock: string;
    startAt: string;
    lockAt: string;
    lockBlock: string;
    lockPrice: string;
    endBlock: string;
    closePrice: string;
    totalBets: string;
    totalAmount: string;
    bearBets: string;
    bullBets: string;
    bearAmount: string;
    bullAmount: string;
    position: string;
    bets: BetResponse[];
}
export interface MarketResponse {
    id: string;
    paused: boolean;
    epoch: {
        epoch: string;
    };
}
export declare const getRoundBaseFields: () => string;
export declare const getBetBaseFields: () => string;
export declare const getUserBaseFields: () => string;
