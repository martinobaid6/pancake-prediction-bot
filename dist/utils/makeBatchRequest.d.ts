declare const makeBatchRequest: (calls: any[]) => Promise<unknown[]>;
export default makeBatchRequest;
