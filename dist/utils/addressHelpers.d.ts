export declare const getAddress: (address: any) => string;
export declare const getPredictionsAddress: () => string;
export declare const getChainlinkOracleAddress: () => string;
