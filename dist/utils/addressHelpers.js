"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getChainlinkOracleAddress = exports.getPredictionsAddress = exports.getAddress = void 0;
const contracts_1 = __importDefault(require("config/constants/contracts"));
exports.getAddress = (address) => {
    const mainNetChainId = 56;
    const chainId = process.env.REACT_APP_CHAIN_ID;
    return address[chainId] ? address[chainId] : address[mainNetChainId];
};
exports.getPredictionsAddress = () => {
    return exports.getAddress(contracts_1.default.predictions);
};
exports.getChainlinkOracleAddress = () => {
    return exports.getAddress(contracts_1.default.chainlinkOracle);
};
//# sourceMappingURL=addressHelpers.js.map