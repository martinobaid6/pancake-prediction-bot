"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPredictionsContract = void 0;
const web3_1 = __importDefault(require("utils/web3"));
const predictions_json_1 = __importDefault(require("config/abi/predictions.json"));
const getContract = (abi, address, web3) => {
    const _web3 = web3 !== null && web3 !== void 0 ? web3 : web3_1.default;
    return new _web3.eth.Contract(abi, address);
};
exports.getPredictionsContract = (web3) => {
    return getContract(predictions_json_1.default, getPredictionsAddress(), web3);
};
//# sourceMappingURL=contractHelpers.js.map