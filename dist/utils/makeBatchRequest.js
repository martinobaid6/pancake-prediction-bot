"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const web3_1 = require("./web3");
const makeBatchRequest = (calls) => {
    try {
        const web3 = web3_1.getWeb3NoAccount();
        const batch = new web3.BatchRequest();
        const promises = calls.map((call) => {
            return new Promise((resolve, reject) => {
                batch.add(call.request({}, (err, result) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(result);
                    }
                }));
            });
        });
        batch.execute();
        return Promise.all(promises);
    }
    catch (_a) {
        return null;
    }
};
exports.default = makeBatchRequest;
//# sourceMappingURL=makeBatchRequest.js.map