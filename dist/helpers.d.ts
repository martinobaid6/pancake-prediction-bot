import { Bet, Market, Round } from 'state/types';
import { BetResponse, RoundResponse, MarketResponse } from './queries';
export declare enum Result {
    WIN = "win",
    LOSE = "lose",
    CANCELED = "canceled",
    LIVE = "live"
}
export declare const numberOrNull: (value: string) => number;
export declare const makeFutureRoundResponse: (epoch: number, startBlock: number) => RoundResponse;
export declare const transformBetResponse: (betResponse: BetResponse) => any;
export declare const transformRoundResponse: (roundResponse: RoundResponse) => any;
export declare const transformMarketResponse: (marketResponse: MarketResponse) => any;
export declare const makeRoundData: (rounds: Round[]) => any;
export declare const getRoundResult: (bet: any, currentEpoch: number) => Result;
export declare const getCanClaim: (bet: any) => boolean;
export declare const getUnclaimedWinningBets: (bets: Bet[]) => Bet[];
export declare const getStaticPredictionsData: () => Promise<{
    status: any;
    currentEpoch: number;
    intervalBlocks: number;
    bufferBlocks: number;
    minBetAmount: any;
}>;
export declare const getMarketData: () => Promise<{
    rounds: Round[];
    market: Market;
}>;
export declare const getRound: (id: string) => Promise<any>;
declare type BetHistoryWhereClause = Record<string, string | number | boolean | string[]>;
export declare const getBetHistory: (where?: BetHistoryWhereClause, first?: number, skip?: number) => Promise<BetResponse[]>;
export declare const getBet: (betId: string) => Promise<BetResponse>;
export {};
