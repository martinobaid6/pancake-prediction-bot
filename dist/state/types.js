"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HistoryFilter = exports.PredictionStatus = exports.BetPosition = void 0;
var BetPosition;
(function (BetPosition) {
    BetPosition["BULL"] = "Bull";
    BetPosition["BEAR"] = "Bear";
    BetPosition["HOUSE"] = "House";
})(BetPosition = exports.BetPosition || (exports.BetPosition = {}));
var PredictionStatus;
(function (PredictionStatus) {
    PredictionStatus["INITIAL"] = "initial";
    PredictionStatus["LIVE"] = "live";
    PredictionStatus["PAUSED"] = "paused";
    PredictionStatus["ERROR"] = "error";
})(PredictionStatus = exports.PredictionStatus || (exports.PredictionStatus = {}));
var HistoryFilter;
(function (HistoryFilter) {
    HistoryFilter["ALL"] = "all";
    HistoryFilter["COLLECTED"] = "collected";
    HistoryFilter["UNCOLLECTED"] = "uncollected";
})(HistoryFilter = exports.HistoryFilter || (exports.HistoryFilter = {}));
//# sourceMappingURL=types.js.map